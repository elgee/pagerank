# pagerank
# An algorithm which is used by google to rank websites.
# It is based on the idea of a random surfer who clicks on links randomly
# This forms a stochastic system where the probabilites of transition 
# between states (web pages) is either zero or 1/number of links on that
# page. This forms the transition matrix which is passed to the function. 
# d is a damping factor which increases the probability of going to a page
# which is not linked to.
def pagerank(transition_matrix, eps=1e-8, d=0.85):
    N = len(transition_matrix)
    pageranks = [1/N for row in transition_matrix]
    while True:
        prev_pageranks = [rank for rank in pageranks]
        converged = True
        for row in range(N):
            pageranks[row] = (1 - d) / N
            for col in range(N):
                pageranks[row] += d * transition_matrix[row][col] *\
                    prev_pageranks[col]
            
            if abs(pageranks[row] - prev_pageranks[row]) > eps:
                converged = False

        if converged:
            break

    return pageranks

def main():
	transition_matrix = \
	[[0, 0, 0, 0, 0],
	 [1/4, 0, 0, 0, 1/3],
	 [1/4, 0, 0, 1/2, 1/3],
	 [1/4, 1, 1, 0, 1/3],
	 [1/4, 0, 0, 1/2, 0]]

	print(pagerank(transition_matrix, d=1))
	print(pagerank(transition_matrix))

if __name__ == '__main__':
    main()
